#lang scribble/manual

@(require
  scribble/example
  (for-label
    racket/base
    racket/contract
    csexp))

@(define evaluator (make-base-eval))
@(examples #:eval evaluator #:hidden (require csexp))

@title[#:style 'toc]{csexp: S-expressions over the network}
@author["Christopher Lemmer Webber" "Jérôme Martin"]

@defmodule[csexp]

Canonical s-expressions (csexp) are a way to format s-expressions into data packets
suitable for transmission over a network.

@section{Overview}

Consider the following expression:

@(racketblock
  '(bag "Inventory" (content cookie caramel aligator)))

To get a canonical s-exp, it must first be changed into a tree of bytestrings:

@(racketblock
  (list #"bag" #"\"Inventory\"" (list #"content" #"cookie" #"caramel" #"aligator")))

Which can then be transmitted as:

@(racketblock
  #"(3:bag11:\"Inventory\"(7:content6:cookie7:caramel8:aligator))")

Spaces are removed and all strings are suffixed by their size and a colon @litchar{:} separator.
This makes it easy to read the expression in a stream, and gives a unique (canonical) way
of writing a given s-expression.

@section{Hinted csexp}

Canonical s-expressions can have an optional "hint", which is a bytestring attached to the front of a
csexp atom, inside square brackets:

@(racketblock
  #"(6:animal(4:name[13:charset=utf-8]7:The Cat)(5:noise4:meow))")

Here the hint is attached to the atom @litchar{The Cat} and has the value @litchar{charset=utf-8}.

When parsing a csexp, hints are rendered by default as @racket[hinted] structures.

@(examples #:eval evaluator
  (bytes->csexp #"(6:animal(4:name[13:charset=utf-8]7:The Cat)(5:noise4:meow))"))

@section{Reference}

@defproc[(csexp->bytes [csexp csexp?]) bytes?]{
  Transform a csexp into a string of bytes.
}

@defproc[(bytes->csexp [bytes bytes?]
                       [#:hinting hinting (or/c 'if-present 'never 'always) 'if-present]) csexp?]{
  Reads a string of bytes and parse it as a csexp.

  If @racket[hinting] is @racket['if-present], it generates a @racket[hinted] instance every time a hint
  is found. @(linebreak)
  If @racket[hinting] is @racket['always], it generates @racket[hinted] instances for each atom, with @racket[#f] as the default hint value. @(linebreak)
  If @racket[hinting] is @racket['never], it ignores any hint and just returns bytestrings.
}

@defproc[(read-csexp [port input-port?]
                     [#:hinting hinting (or/c 'if-present 'never 'always) 'if-present]) csexp?]{
  Read a csexp from a port, byte by byte.
}
@defproc[(write-csexp [csexp csexp?] [port output-port?]) void]{
  Write a csexp to a port.
}

@defstruct*[hinted ([bytestring bytes?] [hint bytes?]) #:transparent]{
  A structure which contains hints. See @secref{Hinted_csexp}.
}

@defproc[(csexp? [v any]) boolean?]{
  Check if a given value is a canonical s-expression.

  @(examples #:eval evaluator
    (csexp? '(#"animal" (#"name" #"cat") (#"noise" #"meow")))
    (csexp? (list #"animal"
            (list (hinted #"name" #"foo") #"cat")
            '(#"noise" #"meow")))
    (csexp? '())
    (csexp? #"aligator")
    (csexp? (list "aligator"))
    (csexp? (list 'aligator)))
}
